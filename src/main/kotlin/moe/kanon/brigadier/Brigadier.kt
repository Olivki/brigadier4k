/*
 * Copyright 2019 Oliver Berg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:Suppress("NOTHING_TO_INLINE")

package moe.kanon.brigadier

import com.mojang.brigadier.CommandDispatcher
import com.mojang.brigadier.RedirectModifier
import com.mojang.brigadier.arguments.ArgumentType
import com.mojang.brigadier.arguments.BoolArgumentType
import com.mojang.brigadier.arguments.DoubleArgumentType
import com.mojang.brigadier.arguments.FloatArgumentType
import com.mojang.brigadier.arguments.IntegerArgumentType
import com.mojang.brigadier.arguments.LongArgumentType
import com.mojang.brigadier.arguments.StringArgumentType
import com.mojang.brigadier.builder.LiteralArgumentBuilder
import com.mojang.brigadier.builder.RequiredArgumentBuilder
import com.mojang.brigadier.context.CommandContext
import com.mojang.brigadier.tree.CommandNode
import com.mojang.brigadier.tree.LiteralCommandNode
import java.util.function.Predicate
import kotlin.reflect.KClass

@Suppress("UNCHECKED_CAST")
object BrigadierLinks {
    @PublishedApi
    @get:JvmSynthetic
    internal val map: MutableMap<KClass<*>, ArgumentType<*>> = HashMap()
    
    inline fun <reified C : Any> register(argumentType: ArgumentType<C>): ArgumentType<C> {
        map[C::class] = argumentType
        return argumentType
    }
    
    operator fun <C : Any> get(clz: KClass<C>): ArgumentType<C> = map.getValue(clz) as ArgumentType<C>
    
    inline operator fun <reified C : Any> invoke(): ArgumentType<C> = map.getValue(C::class) as ArgumentType<C>
    
    init {
        register<Boolean>(BoolArgumentType.bool())
        register<Int>(IntegerArgumentType.integer())
        register<Long>(LongArgumentType.longArg())
        register<Float>(FloatArgumentType.floatArg())
        register<Double>(DoubleArgumentType.doubleArg())
        register<String>(StringArgumentType.string())
    }
}

object Brigadier : DefaultArgumentTypesAdapter()

open class DefaultArgumentTypesAdapter {
    @BrigadierMarker
    inline val int
        get() = IntegerArgumentType.integer()
    
    @BrigadierMarker
    inline fun int(min: Int) = IntegerArgumentType.integer(min)
    
    @BrigadierMarker
    inline fun int(range: IntRange) = IntegerArgumentType.integer(range.start, range.endInclusive)
    
    @BrigadierMarker
    inline val long
        get() = LongArgumentType.longArg()
    
    @BrigadierMarker
    inline fun long(min: Long) = LongArgumentType.longArg(min)
    
    @BrigadierMarker
    inline fun long(range: LongRange) = LongArgumentType.longArg(range.start, range.endInclusive)
    
    @BrigadierMarker
    inline val float
        get() = FloatArgumentType.floatArg()
    
    @BrigadierMarker
    inline fun float(min: Float) = FloatArgumentType.floatArg(min)
    
    @BrigadierMarker
    inline fun float(range: ClosedFloatingPointRange<Float>) =
        FloatArgumentType.floatArg(range.start, range.endInclusive)
    
    @BrigadierMarker
    inline val double
        get() = DoubleArgumentType.doubleArg()
    
    @BrigadierMarker
    inline fun double(min: Double) = DoubleArgumentType.doubleArg(min)
    
    @BrigadierMarker
    inline fun double(range: ClosedFloatingPointRange<Double>) =
        DoubleArgumentType.doubleArg(range.start, range.endInclusive)
    
    @BrigadierMarker
    inline val bool
        get() = BoolArgumentType.bool()
    
    @BrigadierMarker
    inline val string
        get() = StringArgumentType.string()
    
    @BrigadierMarker
    inline val greedyString
        get() = StringArgumentType.greedyString()
    
    @BrigadierMarker
    inline val word
        get() = StringArgumentType.word()
}

@DslMarker
annotation class BrigadierMarker

@BrigadierMarker
class LiteralCommandContainer<S>(val name: String) : DefaultArgumentTypesAdapter() {
    @PublishedApi
    @get:JvmSynthetic
    internal val builder: LiteralArgumentBuilder<S> = LiteralArgumentBuilder.literal(name)
    
    val requirement: Predicate<S>? get() = builder.requirement
    
    val redirect: CommandNode<S>? get() = builder.redirect
    
    val redirectModifier: RedirectModifier<S>? get() = builder.redirectModifier
    
    val isFork: Boolean get() = builder.isFork
    
    val arguments: List<CommandNode<S>> get() = builder.arguments.toList()
    
    // requires
    @BrigadierMarker
    inline fun requires(crossinline predicate: (S) -> Boolean) {
        builder.requires { s -> predicate(s) }
    }
    
    // redirect
    @BrigadierMarker
    inline fun redirectTo(target: CommandNode<S>) {
        builder.redirect(target)
    }
    
    @BrigadierMarker
    inline fun redirectTo(target: CommandNode<S>, crossinline modifier: (CommandContext<S>) -> S) {
        builder.redirect(target) { modifier(it) }
    }
    
    // fork
    @BrigadierMarker
    inline fun forkTo(target: CommandNode<S>, crossinline modifier: (CommandContext<S>) -> List<S>) {
        builder.fork(target) { modifier(it) }
    }
    
    // forward
    @BrigadierMarker
    inline fun forwardTo(target: CommandNode<S>, fork: Boolean, crossinline modifier: (CommandContext<S>) -> List<S>) {
        builder.forward(target, { modifier(it) }, fork)
    }
    
    // literal
    @BrigadierMarker
    inline fun literal(literal: String, closure: LiteralCommandContainer<S>.() -> Unit) {
        builder.then(LiteralCommandContainer<S>(literal).apply(closure).builder)
    }
    
    // argument
    @BrigadierMarker
    inline fun argument(node: CommandNode<S>) {
        builder.then(node)
    }
    
    @BrigadierMarker
    inline fun <reified T : Any> argument(name: String, closure: CommandArgumentContainer<S, T>.() -> Unit) {
        val argumentType = BrigadierLinks<T>()
        builder.then(CommandArgumentContainer<S, T>(name, argumentType).apply(closure).builder)
    }
    
    @BrigadierMarker
    inline fun <reified T : Any> argument(
        name: String,
        argumentType: ArgumentType<T>,
        closure: CommandArgumentContainer<S, T>.() -> Unit
    ) {
        builder.then(CommandArgumentContainer<S, T>(name, argumentType).apply(closure).builder)
    }
    
    /**
     * Runs the specified [closure] whenever this command is invoked.
     *
     * This returns the specific [Int] specified in the [closure] to the brigadier system.
     *
     * @see executes
     */
    @BrigadierMarker
    inline fun executesSpecific(crossinline closure: (CommandContext<S>) -> Int) {
        builder.executes { closure(it) }
    }
    
    /**
     * Runs the specified [closure] whenever this command is invoked.
     *
     * This function returns `1` to the brigadier system.
     *
     * @see executesSpecific
     */
    @BrigadierMarker
    inline fun executes(crossinline closure: (CommandContext<S>) -> Unit) {
        builder.executes {
            closure(it)
            1
        }
    }
    
    // operator
    inline operator fun plusAssign(node: CommandNode<S>) {
        argument(node)
    }
}

@BrigadierMarker
class CommandArgumentContainer<S, T>(val name: String, val argumentType: ArgumentType<T>) :
    DefaultArgumentTypesAdapter() {
    @PublishedApi
    @get:JvmSynthetic
    internal val builder: RequiredArgumentBuilder<S, T> = RequiredArgumentBuilder.argument(name, argumentType)
    
    val requirement: Predicate<S>? get() = builder.requirement
    
    val redirect: CommandNode<S>? get() = builder.redirect
    
    val redirectModifier: RedirectModifier<S>? get() = builder.redirectModifier
    
    val isFork: Boolean get() = builder.isFork
    
    val arguments: List<CommandNode<S>> get() = builder.arguments.toList()
    
    // requires
    @BrigadierMarker
    inline fun requires(crossinline predicate: (S) -> Boolean) {
        builder.requires { s -> predicate(s) }
    }
    
    // redirect
    @BrigadierMarker
    inline fun redirectTo(target: CommandNode<S>) {
        builder.redirect(target)
    }
    
    @BrigadierMarker
    inline fun redirectTo(target: CommandNode<S>, crossinline modifier: (CommandContext<S>) -> S) {
        builder.redirect(target) { modifier(it) }
    }
    
    // fork
    @BrigadierMarker
    inline fun forkTo(target: CommandNode<S>, crossinline modifier: (CommandContext<S>) -> List<S>) {
        builder.fork(target) { modifier(it) }
    }
    
    // forward
    @BrigadierMarker
    inline fun forwardTo(target: CommandNode<S>, fork: Boolean, crossinline modifier: (CommandContext<S>) -> List<S>) {
        builder.forward(target, { modifier(it) }, fork)
    }
    
    // literal
    @BrigadierMarker
    inline fun literal(literal: String, closure: LiteralCommandContainer<S>.() -> Unit) {
        builder.then(LiteralCommandContainer<S>(literal).apply(closure).builder)
    }
    
    // argument
    @BrigadierMarker
    inline fun argument(node: CommandNode<S>) {
        builder.then(node)
    }
    
    @BrigadierMarker
    inline fun <reified T : Any> argument(name: String, closure: CommandArgumentContainer<S, T>.() -> Unit) {
        val argumentType = BrigadierLinks<T>()
        builder.then(CommandArgumentContainer<S, T>(name, argumentType).apply(closure).builder)
    }
    
    @BrigadierMarker
    inline fun <reified T : Any> argument(
        name: String,
        argumentType: ArgumentType<T>,
        closure: CommandArgumentContainer<S, T>.() -> Unit
    ) {
        builder.then(CommandArgumentContainer<S, T>(name, argumentType).apply(closure).builder)
    }
    
    /**
     * Runs the specified [closure] whenever this command is invoked.
     *
     * This returns the specific [Int] specified in the [closure] to the brigadier system.
     *
     * @see executes
     */
    @BrigadierMarker
    inline fun executesSpecific(crossinline closure: (CommandContext<S>) -> Int) {
        builder.executes { closure(it) }
    }
    
    /**
     * Runs the specified [closure] whenever this command is invoked.
     *
     * This function returns `1` to the brigadier system.
     *
     * @see executesSpecific
     */
    @BrigadierMarker
    inline fun executes(crossinline closure: (CommandContext<S>) -> Unit) {
        builder.executes {
            closure(it)
            1
        }
    }
}

fun <S> LiteralCommandContainer<S>.toNode(): LiteralCommandNode<S> = builder.build()

fun <S, T> CommandArgumentContainer<S, T>.toNode(): CommandNode<S> = builder.build()

inline operator fun <S> CommandDispatcher<S>.plusAssign(builder: LiteralArgumentBuilder<S>) {
    this.register(builder)
}

inline operator fun <S> CommandDispatcher<S>.plusAssign(container: LiteralCommandContainer<S>) {
    this.register(container.builder)
}

@BrigadierMarker
inline fun <S> createCommand(
    literal: String,
    closure: LiteralCommandContainer<S>.() -> Unit
): LiteralCommandContainer<S> = LiteralCommandContainer<S>(literal).apply(closure)

@BrigadierMarker
inline fun <S> createLiteral(
    literal: String,
    closure: LiteralCommandContainer<S>.() -> Unit
): LiteralCommandContainer<S> = LiteralCommandContainer<S>(literal).apply(closure)

@BrigadierMarker
inline fun <S, T> createArgument(
    name: String,
    argumentType: ArgumentType<T>,
    closure: CommandArgumentContainer<S, T>.() -> Unit
): CommandArgumentContainer<S, T> = CommandArgumentContainer<S, T>(name, argumentType).apply(closure)

@BrigadierMarker
inline fun <S> CommandDispatcher<S>.register(
    literal: String,
    closure: LiteralCommandContainer<S>.() -> Unit
): LiteralCommandNode<S> = this.register(LiteralCommandContainer<S>(literal).apply(closure).builder)

operator fun <S> CommandDispatcher<S>.contains(name: String): Boolean = this.root.getChild(name) != null

operator fun <S> CommandDispatcher<S>.get(vararg names: String): CommandNode<S> =
    this.findNode(names.asList())
        ?: throw NoSuchElementException("No child with the name <$names> found in command <${this.root.name}>")

// command node

inline operator fun <S> CommandNode<S>.plusAssign(node: CommandNode<S>) {
    this.addChild(node)
}

inline operator fun <S, T> CommandNode<S>.plusAssign(container: CommandArgumentContainer<S, T>) {
    this.addChild(container.toNode())
}

operator fun <S> CommandNode<S>.contains(name: String): Boolean = this.getChild(name) != null

operator fun <S> CommandNode<S>.get(name: String): CommandNode<S> =
    this.getChild(name)
        ?: throw NoSuchElementException("No child with the name <$name> found in command <${this.name}>")

// command context
inline fun <reified C : Any> CommandContext<*>.getArgument(name: String): C = this.getArgument(name, C::class.java)

inline operator fun <reified C : Any> CommandContext<*>.get(name: String): C = this.getArgument(name)

inline operator fun <reified C : Any> CommandContext<*>.invoke(name: String): C = this.getArgument(name)