## brigadier4k

bridgadier4k is a "wrapper" library around the [Brigadier](https://github.com/Mojang/brigadier) command parsing library made by Mojang, for Kotlin, to make it more "idiomatic" to use with Kotlin.

## Installation

Gradle

- Groovy

  ```groovy
  repositories {
      maven { url "https://dl.bintray.com/olivki/kotlin" }
  }
  
  dependencies {
      compile "moe.kanon.brigadier:brigadier4k:LATEST_VERSION"
  }
  ```

- Kotlin

  ```kotlin
  repositories {
  	maven(url = "https://dl.bintray.com/olivki/kotlin")
  }
  
  dependencies {
      compile(group = "moe.kanon.brigadier", name = "brigadier4k", version = "LATEST_VERSION")
  }
  ```

Maven

```xml
<dependency>
    <groupId>moe.kanon.brigadier</groupId>
    <artifactId>brigadier4k</artifactId>
    <version>LATEST_VERSION</version>
    <type>pom</type>
</dependency>

```

## License

````
Copyright 2019 Oliver Berg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
````